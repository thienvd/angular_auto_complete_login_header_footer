import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
 
  title = 'angularpopup';
  showModal:any;
  registerForm:any;
  submitted = false;


  constructor(private formBuilder: FormBuilder) { }

  show()
  {
    this.showModal = true; // Show-Hide Modal Check
    
  }
  //Bootstrap Modal Close event
  hide()
  {
    this.showModal = false;
  }

  ngOnInit(): void {

  this.registerForm = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(6)]],
    firstname: ['', [Validators.required, Validators.minLength(6)]],
    mobile: ['', [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/), Validators.minLength(10)]]
});
}
// convenience getter for easy access to form fields
get f() { return this.registerForm.controls; }

onSubmit() {
this.submitted = true;
// stop here if form is invalid
if (this.registerForm.invalid) {
    return;
}
if(this.submitted)
{
  this.showModal = false;
}

}

}
