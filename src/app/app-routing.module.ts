import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutoSearchComponent } from './auto-search/auto-search.component';
import { AutoComponent } from './auto/auto.component';
import { BookComponent } from './book/book.component';
import { HomeComponent } from './home/home.component';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { LoginComponent } from './login/login.component';
import { StepComponent } from './step/step.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'book', component: BookComponent },
  { path:'step',component:StepComponent},
  { path:'land', component:LandingpageComponent},
  { path:'auto-search', component:AutoSearchComponent},
  { path:'auto', component:AutoComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
