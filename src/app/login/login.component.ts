import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

// @Component({
//   selector: 'app-login',
//   templateUrl: './login.component.html',
//   styleUrls: ['./login.component.css']
// })

export class LoginComponent implements OnInit {

  public userForm: FormGroup;
  constructor(private formBuilder: FormBuilder) 
  {
    this.userForm = this.formBuilder.group({
      address: this.formBuilder.array([this.addAddressGroup()])
    });

   }

   private addAddressGroup(): FormGroup {
    return this.formBuilder.group({
      street: [],
      city: [],
      state: []
    });
  }
  //Add Fields
  addAddress(): void {
    this.addressArray.push(this.addAddressGroup());
  }
 
  //Remove Fields
  removeAddress(index: number): void {
    this.addressArray.removeAt(index);
  }
  //Fields Array
  get addressArray(): FormArray {
    return <FormArray>this.userForm.get('address');
  }   

   show()
   {
     this.showModal = true; // Show-Hide Modal Check
     
   }
   //Bootstrap Modal Close event
   hide()
   {
     this.showModal = false;
   }
  //
  //Form Validables 
  registerForm:any =  FormGroup;
  submitted = false;
  showModal:any;
  //Add user form actions
  get f() { return this.registerForm.controls; }
 
  onSubmit() {
    
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
        return;
    }
    //True if all the fields are filled
    if(this.submitted)
    {
     let fname=  this.registerForm.controls['firstname'].value;
     let nEmail = this.registerForm.controls['email'].value;
     let nMobile = this.registerForm.controls['mobile'].value;
     let nPass = this.registerForm.controls['password'].value;
     
      console.log(fname);
      console.log(nEmail);
      console.log(nMobile);
      console.log(nPass);
    }
  
  }  
   
    //login form

    
  ngOnInit(): void {
    //login form
   //Add User form validations
  //  this.registerForm = this.formBuilder.group({
  //   email: ['', [Validators.required, Validators.email]],
  //   password: ['', [Validators.required]],
  //   });


  this.registerForm = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(6)]],
    firstname: ['', [Validators.required, Validators.minLength(6)]],
    mobile: ['', [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/), Validators.minLength(10)]]
});


  }

}
