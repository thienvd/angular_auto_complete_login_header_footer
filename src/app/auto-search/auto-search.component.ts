import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ThisReceiver } from '@angular/compiler';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';



export interface State {
  id:number;
  flag: string;
  name: string;
  population: string;
}

export interface Employee {
  id: number;
  name: string;
  jobtype: string;
  email: string;
  address: string;
  imageUrl: string;
}


@Component({
  selector: 'app-auto-search',
  templateUrl: './auto-search.component.html',
  styleUrls: ['./auto-search.component.css']
})


export class AutoSearchComponent implements OnInit {


  ngOnInit(): void {
    this.filterValue = "AAA";
  }

  filterValue:any;
  stateCtrl = new FormControl();
  filteredStates: Observable<State[]>;
  states: State[] =
    // [
    //   {
    //     name: 'Arkansas',
    //     population: '2.978M',
    //     // https://commons.wikimedia.org/wiki/File:Flag_of_Arkansas.svg
    //     flag: 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg',
    //   },
    //   {
    //     name: 'California',
    //     population: '39.14M',
    //     // https://commons.wikimedia.org/wiki/File:Flag_of_California.svg
    //     flag: 'https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg',
    //   },
    //   {
    //     name: 'Florida',
    //     population: '20.27M',
    //     // https://commons.wikimedia.org/wiki/File:Flag_of_Florida.svg
    //     flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Florida.svg',
    //   },
    //   {
    //     name: 'Texas',
    //     population: '27.47M',
    //     // https://commons.wikimedia.org/wiki/File:Flag_of_Texas.svg
    //     flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Texas.svg',
    //   },
    // ];



    [
      {
        id:1,
        name: "Cá tầm Việt Nam",
        flag: "http://cdn.smartlifevn.com/upload/ecommerce/2018/09/21/c2e57ef9-a3e4-4152-9952-d77fed70abd7.png",
        population: ''

      },
      {
        id:2,
        name: "Trường Food",
        flag: "http://cdn.smartlifevn.com/upload/ecommerce/2018/09/21/0a03e3c8-703f-475b-b7ea-686971747f67.png",
        population: ''
      },
      {
        id:3,
        name: "Eurolife ",
        flag: "http://cdn.smartlifevn.com/upload/ecommerce/2018/09/21/030fb933-f2d3-4d7a-997e-c40112505ba8.png",
        population: ''

      },
      {
        id:4,
        name: "Nông sản thực phẩm sạch Tây Bắc ",
        flag: "http://cdn.smartlifevn.com/upload/ecommerce/2018/09/22/c94031cd-9e8b-4768-be9e-cf423784dadf.jpg",
        population: ''

      },
      {
        id:5,
        name: "Hiệp hội Nho và Táo Ninh Thuận",
        flag: "https://www.barrowboysiw.co.uk/wp-content/uploads/2017/02/2016_1_15_nhox.jpg",
        population: ''

      },
      {
        id:6,
        name: "Tiến Lĩnh",
        flag: '',
        population: ''

      },
      {
        id:7,
        name: "Hàm Yên",
        flag: "http://cdn.smartlifevn.com/upload/ecommerce/2018/09/24/76cd374d-d981-400c-a83c-89f99b03b081.png",
        population: ''
      },
      {
        id:8,
        name: "Phong Hưởng",
        flag: "http://cdn.smartlifevn.com/upload/ecommerce/2018/09/24/220d9aec-5bf4-4d61-a237-7cda04852c35.jpg",
        population: ''

      },

    ]

  constructor() {
    this.filteredStates = this.stateCtrl.valueChanges.pipe(
      startWith(''),
      map(state => (state ? this._filterStates(state) : this.states.slice())),
    );

    this.filteredEmployee = this.employeeCtrl.valueChanges
      .pipe(
        startWith(''),
        map(employee => employee ? this._filterEmployees(employee) : this.employees.slice())
      );
  }
  private _filterStates(value: string): State[] {
    this.filterValue= value.toLowerCase();   
    //var find_object = this.states.find(state => state.name == this.filterValue.toLowerCase());   
    //console.log(value.id)
    return this.states.filter(state => state.name.toLowerCase().includes(this.filterValue));
  }


 public selectOption(e: MatAutocompleteSelectedEvent) {
    const item: State = e.option.value;
    console.log(item);
 }


 private _filterEmployees(value: string): Employee[] {
  
  const filter = value.toLowerCase();

  return this.employees.filter(state => state.name.toLowerCase().indexOf(filter) === 0);
}




 employeeCtrl = new FormControl();
 filteredEmployee: Observable<Employee[]>;

 employees: Employee[] = [
   {
     "id": 1,
     "name": "Lilla Nitzsche",
     "jobtype": "Human Intranet Liaison",
     "email": "Edwardo_Homenick@gmail.com",
     "address": "11360 Bahringer Squares",
     "imageUrl": "https://s3.amazonaws.com/uifaces/faces/twitter/aleclarsoniv/128.jpg"
   },
   {
     "id": 2,
     "name": "Francisca Considine",
     "jobtype": "Dynamic Web Supervisor",
     "email": "Don_Ernser@yahoo.com",
     "address": "852 Lori Turnpike",
     "imageUrl": "https://s3.amazonaws.com/uifaces/faces/twitter/hiemil/128.jpg"
   },
   {
     "id": 3,
     "name": "Gussie Stoltenberg",
     "jobtype": "Customer Intranet Representative",
     "email": "Leonard.Turcotte17@hotmail.com",
     "address": "10080 Jett Corners",
     "imageUrl": "https://s3.amazonaws.com/uifaces/faces/twitter/necodymiconer/128.jpg"
   },
   {
     "id": 4,
     "name": "Fatima Dickinson",
     "jobtype": "Dynamic Directives Orchestrator",
     "email": "Woodrow_Carter97@gmail.com",
     "address": "3249 Wilkinson Overpass",
     "imageUrl": "https://s3.amazonaws.com/uifaces/faces/twitter/pifagor/128.jpg"
   },
   {
     "id": 5,
     "name": "Edmund Nicolas MD",
     "jobtype": "Future Markets Designer",
     "email": "Garland_Carter@yahoo.com",
     "address": "35163 McKenzie Forest",
     "imageUrl": "https://s3.amazonaws.com/uifaces/faces/twitter/michaelbrooksjr/128.jpg"
   },
 ];



  brandSelected(e:any, item:State) {

  // if (e.source.selected) {
  //  this.brandId = item.id;
   // this.product.brand.id= item.id;
     console.log(item.id);
  // }
}



}
