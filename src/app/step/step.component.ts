import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-step',
  templateUrl: './step.component.html',
  styleUrls: ['./step.component.css']
})
export class StepComponent implements OnInit 
{



  constructor(private formBuilder: FormBuilder)
   { }
    personalDetails: any;
    addressDetails:any;
    educationalDetails: any;
    personal_step = false;
    address_step = false;
    education_step = false;
    step = 1;
   
   

  ngOnInit(): void {


  this.personalDetails = this.formBuilder.group({      
      name: ['', [Validators.required,Validators.maxLength(20)]],
      email: ['', Validators.required],
      phone: ['',Validators.required]
  });

  this.addressDetails = this.formBuilder.group({
      city: ['', Validators.required],
      address: ['', Validators.required],
      pincode: ['',Validators.required]
  });

  this.educationalDetails = this.formBuilder.group({
      highest_qualification: ['', Validators.required],
      university: ['', Validators.required],
      total_marks: ['',Validators.required]
  });
   

  
  }
  public get personal() { return this.personalDetails.controls; }
    
  public  get address() { return this.addressDetails.controls; }
  
  public  get education() { return this.educationalDetails.controls; }
   
  next(){
 
      if(this.step==1){
            this.personal_step = true;
            if (this.personalDetails.invalid) { return  }
            this.step++
      }
  
      else if(this.step==2){
          this.address_step = true;
          if (this.addressDetails.invalid) { return }
              this.step++;
      }      
  
    }
  
  previous(){
      this.step--
     
      if(this.step==1){
        this.address_step = false;
      }
      if(this.step==2){
        this.education_step = false;
      }
     
    }  
  submit(){
      
      if(this.step==3){
        this.education_step = true;
        if (this.educationalDetails.invalid) { return }
        
        console.log(this.personalDetails.controls['name'].value)
        console.log(this.personalDetails.controls['email'].value)
        console.log(this.personalDetails.controls['phone'].value)



        console.log(this.educationalDetails.controls['highest_qualification'].value)
        console.log(this.educationalDetails.controls['university'].value)
        console.log(this.educationalDetails.controls['total_marks'].value)
        
        
        alert("Well done!! Hết step rồi nhé")
      }
    }

}
